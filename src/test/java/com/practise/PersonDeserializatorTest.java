package com.practise;

import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class PersonDeserializatorTest {

    @Test
    public void test_deserialize() throws Exception {

        Person serialized = new Person("Joe", 21);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Serializator.<Person>serialize(serialized, os);

        InputStream is = new ByteArrayInputStream(os.toByteArray());
        Person deserialized = PersonDeserializator.deserialize(is);

        assertThat(deserialized, equalTo(serialized));
    }
}
