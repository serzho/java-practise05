package com.practise;

import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class SerializatorTest {

    @Test
    public void test_serialize() throws IOException {
        Person person = new Person("Joe", 21);
        OutputStream os = new ByteArrayOutputStream();
        Serializator.<Person>serialize(person, os);

        assertThat(os.toString(), containsString("name=" + person.name));
        assertThat(os.toString(), containsString("age=" + person.age));
    }
}
