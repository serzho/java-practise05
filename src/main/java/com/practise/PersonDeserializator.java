package com.practise;

import java.io.*;
import java.util.regex.*;

public class PersonDeserializator {

    public static Person deserialize(InputStream is) throws IOException {

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Serializator.DEFAULT_ENCODING));
        String serialized = "";
        String line = "";
        while ((line = reader.readLine()) != null) {
            serialized += line;
        }

        Pattern pattern = Pattern.compile("(\\w+=\\w+)");
        Matcher matcher = pattern.matcher(serialized);
        String deserialized = "";
        Person person = new Person();
        while(matcher.find()) {
            String pair = matcher.group();
            String[] splittedPair = pair.split("=");
            String key = splittedPair[0];
            switch (key) {
                case "name": person.name = splittedPair[1];
                             break;
                case "age": person.age = Integer.parseInt(splittedPair[1]);
                            break;
                default: continue;
            }
        }
        return person;
    }
}
