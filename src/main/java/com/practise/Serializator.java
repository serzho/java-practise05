package com.practise;

import java.io.*;
import java.util.regex.*;
import java.beans.*;
import java.lang.reflect.*;

public class Serializator {

    public static String DEFAULT_ENCODING = "utf8";

    public static <T> void serialize(T object, OutputStream os) throws IOException {
        try {
            String serialized = "";
            for (PropertyDescriptor pd : Introspector.getBeanInfo(object.getClass()).getPropertyDescriptors()) {

                Method method = pd.getReadMethod();
                if (method.getDeclaringClass().equals(object.getClass())) {
                    String methodName = method.getName();
                    String name = methodName.replace("get", "").toLowerCase();
                    serialized += "" + name + "=" + method.invoke(object, null) + ";";
                }
            }
            os.write(serialized.getBytes(DEFAULT_ENCODING));
            os.close();
        } catch (IntrospectionException exp) {
            throw new RuntimeException("Can't serialize object: " + object.toString());
        } catch (IllegalAccessException exp) {
            throw new RuntimeException("Can't serialize object: " + object.toString());
        } catch (InvocationTargetException exp) {
            throw new RuntimeException("Can't serialize object: " + object.toString());
        }
    }
}
