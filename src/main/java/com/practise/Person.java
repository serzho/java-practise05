package com.practise;

import java.io.*;
import java.util.regex.*;

public class Person {

    protected String name;
    protected int age;

    public Person() {}

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static Person deserialize(InputStream is) throws IOException {

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is));
        String serialized = "";
        String line = "";
        while ((line = reader.readLine()) != null) {
            serialized += line;
        }

        Pattern pattern = Pattern.compile("(\\w+=\\w+)");
        Matcher matcher = pattern.matcher(serialized);
        String deserialized = "";
        Person person = new Person();
        while(matcher.find()) {
            String pair = matcher.group();
            String[] splittedPair = pair.split("=");
            String key = splittedPair[0];
            switch (key) {
                case "name": person.name = splittedPair[1];
                             break;
                case "age": person.age = Integer.parseInt(splittedPair[1]);
                            break;
                default: continue;
            }
        }
        return person;
    }

    public String toString() {
        return "Person(name=" + name + ", age=" + age + ")";
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!other.getClass().equals(this.getClass())) return false;
        Person t = (Person) other;
        if (!t.name.equals(this.name)) return false;
        if (!(t.age == this.age)) return false;
        return true;
    }
}
